FROM debian:buster
ENV PYTHONUNBUFFERED 1
RUN apt update -y
RUN apt install -y\
	make
RUN apt install -y\
	build-essential
RUN apt install -y\
	libssl-dev
RUN apt install -y\
	zlib1g-dev
RUN apt install -y\
	libbz2-dev
RUN apt install -y\
	libreadline-dev
RUN apt install -y\
	libsqlite3-dev
RUN apt install -y\
	wget
RUN apt install -y\
	curl
RUN apt install -y\
	llvm
RUN apt install -y\
	libncurses5-dev
RUN apt install -y\
	libncursesw5-dev
RUN apt install -y\
	xz-utils
RUN apt install -y\
	tk-dev
RUN wget https://www.python.org/ftp/python/3.7.10/Python-3.7.10.tgz
RUN tar xvf Python-3.7.10.tgz
RUN cd Python-3.7.10 &&\
      	./configure --enable-optimizations --with-ensurepip=install && \
	make -j 8
RUN cd Python-3.7.10 &&\
	make altinstall
#RUN ./configure --enable-optimizations --with-ensurepip=install
#RUN make -j 8
RUN python3.7 --version
#COPY . .
#RUN python -m pip install --upgrade pip
#RUN pip install -r requirements.txt

